#!/bin/bash

WEBDIR='/mnt/dfs/nmjxv3/userweb/nmjxv3'
CLASS='cpe319'

IFS=$'\n'


for dir in `ls`
do
	if [ -d "$dir" ]; then
		pushd "$dir"
		xelatex "$dir.tex"

		mkdir -p "$WEBDIR/homework/$CLASS/"
		cp "$dir.pdf" "$WEBDIR/homework/$CLASS/"
		
		pushd "$WEBDIR/homework/$CLASS"
		git add "$dir.pdf"
		popd

		popd
	fi
done

pushd "$WEBDIR/homework/$CLASS"
git commit -am "Updating $CLASS homework"
git push
popd

